import net.skladanowski.Sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Class for parameterized test of sort() method from Sorting class
 */
@RunWith(Parameterized.class)
public class ValidArraysSortTest {

    Sorting sorting = new Sorting();

    ArrayList<Integer> testArrayList;
    ArrayList<Integer> expectedArrayList;

    /**
     * Constructor accepting two ArrayLists of Integers
     * @param testArrayList - ArrayList with values to sort
     * @param expectedArrayList - ArrayList with values in order expected to achieve after calling sort() method.
     */
    public ValidArraysSortTest(ArrayList<Integer> testArrayList, ArrayList<Integer> expectedArrayList ) {
        this.testArrayList = testArrayList;
        this.expectedArrayList = expectedArrayList;
    }

    @Parameterized.Parameters
    public static List<Object[]> testSet() {
        return Arrays.asList(new Object[][] {
                {new ArrayList<>(Arrays.asList(9, 5, 2, 7, 15, 93, -18)),
                        new ArrayList<>(Arrays.asList(-18, 2, 5, 7, 9, 15, 93))},
                {new ArrayList<>(Arrays.asList(-98, -400, 7, 0)),
                        new ArrayList<>(Arrays.asList(-400, -98, 0, 7))},
                {new ArrayList<>(Arrays.asList(19, 6, 2, 177)),
                        new ArrayList<>(Arrays.asList(2, 6, 19, 177))},
                {new ArrayList<>(Arrays.asList(-1586, 1586, 997, -998)),
                        new ArrayList<>(Arrays.asList(-1586, -998, 997, 1586))}
        });
    }

    @Test
    public void testSort() {
//        System.out.println("Parameterized Test start:");
        assertEquals(expectedArrayList, sorting.sort(testArrayList));
//        System.out.println("Parameterized Test end.");
    }


}
