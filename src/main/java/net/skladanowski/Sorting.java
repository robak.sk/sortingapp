package net.skladanowski;

import java.util.ArrayList;

/**
 * Class executing sorting algorithm on given array of integers.
 * sort() method accepts array of integers of given length,
 * implements selection sort algorithm and returns array
 * sorted in ascending order.
 * Method throws IllegalArgumentExceptions when argument passed
 * is null, empty, with size out of range (consist less elements than 2
 * ar is longer than 10) as well as when any element of an array is not Integer type.
 */
public class Sorting {

    public ArrayList<Integer> sort(ArrayList<Integer> array) throws IllegalArgumentException {
        if (array == null) {
            throw new IllegalArgumentException("You have call method without an array to sort.");
        } else if (array.isEmpty()) {
            throw new IllegalArgumentException("Your array is empty - nothing to sort.");
        }else if (array.size() == 1) {
            throw new IllegalArgumentException("Your array consist only 1 element - nothing to sort.");
        }else if (array.size() > 10) {
            throw new IllegalArgumentException("Length of your array must not exceed 10 elements.");
        }else {
            for (int i = 0; i < array.size(); i++) {
                for (int j = i + 1; j < array.size(); j++) {
                    int temporary;
                    if (array.get(i) > array.get(j)) {
                        temporary = array.get(i);
                        array.set(i, array.get(j));
                        array.set(j, temporary);
                    }
                }
            }
        }
        return array;
    }
}
