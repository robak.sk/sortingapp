import net.skladanowski.Sorting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Parameterized Test class to assure IllegalArgumentExceptions is thrown
 * for following corner cases:
 * a) sort() called without argument,
 * b) sort() called with empty array,
 * c) sort() called with single element array,
 * d) sort() called with an array of size more than 10
 */
@RunWith(Parameterized.class)
public class InvalidArraysSortTest {

    Sorting sorting = new Sorting();

    ArrayList<Integer> testArrayList;

    /**
     * Constructor accepting single ArrayList of Integers
     */
    public InvalidArraysSortTest(ArrayList<Integer> testArrayList) {
        this.testArrayList = testArrayList;
    }

    @Parameterized.Parameters
    public static List<Object[]> testInvalidSet() {
        return Arrays.asList(new Object[][] {
                {null},
                {new ArrayList<>(Collections.emptyList())},
                {new ArrayList<>(Collections.singletonList(1))},
                {new ArrayList<>(Arrays.asList(-400, -98, 0, 7, 19, 6, 2, 177, 1586, 997, -998))}
        });
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSort() {
        sorting.sort(testArrayList);
    }
}
