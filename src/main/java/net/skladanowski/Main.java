package net.skladanowski;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Main class of the program executing program.
 * Ask user to provide number of integers intended to sort
 * and start do-while loop asking to input same number
 * of integers as declared by the user to store them
 * in "ArrayList<Integer>".
 * Assurance of data type inputted by user implemented
 * Next refers to "sort" method of the "Sorting" class
 * and receives sorted array to print each array element
 * to console.
 */
public class Main {
    /**
     * Standard Java constructor for Main class.
     */
    public static void main(String[] args) throws InterruptedException {

        //setting variables
        Scanner scanner = new Scanner(System.in);
        Sorting sorting = new Sorting();

        ArrayList<Integer> arrayToSort = new ArrayList<>();
        int arrayLength;

        //defining length of array to sort and assure proper data type is inserted
        // to define size of ArrayList and the value is within expected range
        System.out.print("\nHow many integers are you going to sort (min: 2, max: 10)?  : ");
        arrayLength = scanner.nextInt();

        // asking for data in required type
        int userInputNo = 1;

        // do-while loop to collect declared quantity of integers in proper type.
        do {
            System.out.print("Provide value no " + userInputNo + ": ");
            try {
                int userInput = scanner.nextInt();
                arrayToSort.add(userInput);
                userInputNo++;
            } catch (InputMismatchException e) {
                System.out.println("\tWrong value - only integers allowed - try again!");
                scanner.next();
            }
        } while (userInputNo <= arrayLength);

        //call sorting method for array of integers entered by user
        sorting.sort(arrayToSort);

        //printing numbers sorted in ascending order
        System.out.print("Yor numbers sorted in ascending order:\n\t");

        for (int i = 0; i < arrayLength ; i++) {
            System.out.print(arrayToSort.get(i));
            if (i < arrayLength - 1) {
                System.out.print(", ");
            }
        }

        Thread.sleep(1000);
        System.out.println("\n\nGood bye ;-)\n");
    }
}